/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocjp_deber3;

/**
 *
 * @author Mayra
 */
public class DatosPrimitivos {
    
    byte bt;
    short st;
    int it;
    long lg;
    float ft;
    double db;
    boolean bl;
    char ch;

    public DatosPrimitivos() {
    }

    public byte getBt() {
        return bt;
    }

    public void setBt(byte bt) {
        this.bt = bt;
    }

    public short getSt() {
        return st;
    }

    public void setSt(short st) {
        this.st = st;
    }

    public int getIt() {
        return it;
    }

    public void setIt(int it) {
        this.it = it;
    }

    public long getLg() {
        return lg;
    }

    public void setLg(long lg) {
        this.lg = lg;
    }

    public float getFt() {
        return ft;
    }

    public void setFt(float ft) {
        this.ft = ft;
    }

    public double getDb() {
        return db;
    }

    public void setDb(double db) {
        this.db = db;
    }

    public boolean isBl() {
        return bl;
    }

    public void setBl(boolean bl) {
        this.bl = bl;
    }

    public char getCh() {
        return ch;
    }

    public void setCh(char ch) {
        this.ch = ch;
    }
    
    public void asigByte() {
        st = bt;
        it = bt;
        lg = bt;
        ft = bt;
        db = bt;
    }
       
    public void asigChar() {
        it = ch;
        lg = ch;
        ft = ch;
        db = ch;        
    }
    
    public void asigShort() {
        it = st;
        lg = st;
        ft = st;
        db = st;        
    }
    
    public void asigInt() {
        lg = it;
        ft = it;
        db = it; 
    }
    
    public void asigLong() {       
        ft = lg;
        db = lg; 
    }
    
    public void asigDouble() {               
        db = ft;        
    }
    
    public void castDouble() {
        bt = (byte) db;
        ch = (char) db;
        st = (short) db;
        it = (int) db;
        lg = (long) db;
        ft = (float) db;
    }
    
    public void castFloat() {
        bt = (byte) ft;
        ch = (char) ft;        
        st = (short) ft;
        it = (int) ft;
        lg = (long) ft;
    }
    
    public void castLong() {
        bt = (byte) lg;
        ch = (char) lg;
        st = (short) lg;
        it = (int) lg;        
    }
    
    public void castInt() {
        bt = (byte) it;
        ch = (char) it;
        st = (short) it;
    }
    
    public void castChar() {
        bt = (byte) ch;
        st = (short) ch;
    }
    
    public void castShort() {
        bt = (byte) st;
        ch = (char) st;
    }
    
    public void castByte() {
        ch = (char) bt;
    }
}
