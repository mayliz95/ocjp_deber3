/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocjp_deber3;

/**
 *
 * @author Mayra
 */
public class Operadores {
    
    int a;
    int b;
    boolean bol;

    public Operadores() {
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public boolean isBol() {
        return bol;
    }

    public void setBol(boolean bol) {
        this.bol = bol;
    }        

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
        
    public void usoOperadoresUnarios() {
        
        System.out.println("Número normal " + a);
        int b = -a;
        System.out.println("Uso operador (-):  " + b);        
        a++;
        System.out.println("Uso operador (variable++) " + a);
        System.out.println("Uso operador (variable++) " + a++);
        ++a;
        System.out.println("Uso operador (++variable) " + a);
        System.out.println("Uso operador (++variable) " + ++a);
        a--;
        System.out.println("Uso operador (variable--) " + a);
        System.out.println("Uso operador (variable--) " + a--);
        --a;
        System.out.println("Uso operador (--variable) " + a);
        System.out.println("Uso operador (--variable) " + --a);
        
        System.out.println("\nValor norma boolean es " + bol);
        System.out.println("Uso operador (!) \nValor boolean es " + !bol);
        
        byte s = 0;
        
        System.out.println("\nValor normal byte es " + s);
        System.out.println("Uso operador (~) \nValor byte es " + ~s);                
    }     
    
    public void usoOperadoresBinarios() {
        System.out.println("Uso operador (+)\n " + a + " + " + b + " = " + (a+b));
        System.out.println("Uso operador (-)\n" + a + " - " + b + " = " + (a-b));
        System.out.println("Uso operador (*)\n" + a + " * " + b + " = " + (a*b));
        System.out.println("Uso operador (/)\n" + a + " / " + b + " = " + (a/b));
        System.out.println("Uso operador (%)\n" + a + " % " + b + " = " + (a%b));
    }
    
    public void usoOperadoresComparacion() {
        
        System.out.println("a = " + a);           
        System.out.println("b = " + b);
        System.out.println("boolean = " + b);
        
        if (a > b) {
            System.out.println("a es mayor que b");            
        } else if (a < b) {
            System.out.println("b es mayor que a");            
        }
        
        if (a >= b) {
            System.out.println("a es mayor o igual que b");            
        } else if (a <= b) {
            System.out.println("b es mayor o igual que a");            
        }
        
        if (a < b & a < 0) {
            System.out.println("a es menor que b y es un número negativo");            
        } if (a > b && a > 0) {
            System.out.println("a es mayor que b y es un número positivo");            
        }      
        
        if (a >=10 || a < 0) {
            System.out.println("No son número digitos positivos"); 
        } else {
            System.out.println("Son número digitos positivos");          
        }        
        
        if (b > 0 | bol==true) {
            System.out.println("El dato se acepta");
        }
    }
}
